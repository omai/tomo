FROM ruby:2.5.3
RUN apt-get update -qq && apt-get install -y build-essential libpq-dev && curl -sL https://deb.nodesource.com/setup_11.x | bash - && apt-get install -y nodejs && curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add - && echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list && apt-get update && apt-get install yarn
RUN mkdir /omae
WORKDIR /omae
COPY Gemfile /omae/Gemfile
COPY Gemfile.lock /omae/Gemfile.lock
COPY package.json /omae/package.json
COPY yarn.lock  /omae/yarn.lock
RUN bundle install && yarn install
COPY . /omae
CMD ["./bin/bundle exec rails s"]