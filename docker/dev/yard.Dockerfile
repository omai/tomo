FROM registry.gitlab.com/omai/tomo/rails:0.0.1-pre
EXPOSE 8809
CMD ["./bin/bundle", "exec yard server -p 8809 -r"]