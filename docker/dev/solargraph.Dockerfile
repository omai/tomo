FROM registry.gitlab.com/omai/tomo/rails:0.0.1-pre
EXPOSE 7658
CMD ["./bin/bundle exec solargraph socket"]