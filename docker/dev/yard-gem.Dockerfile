FROM registry.gitlab.com/omai/tomo/rails:0.0.1-pre
EXPOSE 8808
CMD ["./bin/bundle exec yard server", " --gems"]