FROM registry.gitlab.com/omai/tomo/rails:0.0.1-pre
EXPOSE 3035
CMD ["./bin/webpack-dev-server", "--host 127.0.0.1"]