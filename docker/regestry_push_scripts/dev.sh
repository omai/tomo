#!/bin/zsh
# Ok we are assuming we are running in the parent directory ... in Tomo proper
docker build -f ./docker/dev/rails.Dockerfile -t registry.gitlab.com/omai/tomo/rails:$1 .
docker push registry.gitlab.com/omai/tomo/rails:$1
docker build -f ./docker/dev/solargraph.Dockerfile -t registry.gitlab.com/omai/tomo/solargraph:$1 .
docker push registry.gitlab.com/omai/tomo/solargraph:$1
docker build -f ./docker/dev/webpacker.Dockerfile -t registry.gitlab.com/omai/tomo/webpacker:$1 .
docker push registry.gitlab.com/omai/tomo/webpacker:$1
docker build -f ./docker/dev/yard.Dockerfile -t registry.gitlab.com/omai/tomo/yard:$1 .
docker push registry.gitlab.com/omai/tomo/yard:$1
docker build -f ./docker/dev/yard-gem.Dockerfile -t registry.gitlab.com/omai/tomo/yard/gem:$1 .
docker push registry.gitlab.com/omai/tomo/yard/gem:$1