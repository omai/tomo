json.extract! contact, :id, :name, :location, :archetype, :data, :visible, :player_custom, :created_at, :updated_at
json.url contact_url(contact, format: :json)
