json.extract! character_contact, :id, :contact_id, :character_id, :loyalty, :created_at, :updated_at
json.url character_contact_url(character_contact, format: :json)
