class Character < ApplicationRecord
    has_many :character_contact, dependent: :delete_all
    has_many :character_attribute, dependent: :delete_all
    has_many :character_skill, dependent: :delete_all
    has_many :character_quality, dependent: :delete_all
    has_many :character_item, dependent: :delete_all
    require 'json'
    #
    # @!attribute name
    #   @return [String] Name of the character
    # @!attribute ailias
    #   @return [String] Alias the character goes by
    # @!attribute images
    #   @return [Array<String>] Array of base64 formated images
    # @!attribute c_attributes
    #   @return [Hash<String, CharacterAttribute>] Hash of attributes associated with the character, with the name as the key
    # @!attribute skills
    #   @return [Hash<String, CharacterSkill>] Hash of skills associated with the character, with the name as the key
    # @!attribute qualities
    #   @return [Array<Array<String, CharacterQuality>>] Hash of qualities associated with the character, with the name as the key
    # @!attribute data
    #   @return [String] Unprocessed portion of the XML String
    #
    serialize :images, Array

    #
    # Converts the Character to a .chum5 string, ready to download
    #
    # @return [String] Chum5 Formated String
    #
    def to_chum5()
        puts "Exporting Chummer File"
        # Parsing Whatever Data we have left
        doc=Nokogiri::XML(data)
        Nokogiri::XML::Builder.with(doc.at_xpath('.//character')) do |xml|
            # Inserting Mugshots
            xml.mugshots{
                images.each{|image|
                    xml.mugshot image
                }
            }
            # Inserting All Contacts
            xml.contacts character_contact.map{|cc| cc=cc.to_chum5_xml()}.join("\n")
        end
        doc.to_xml
    end
    class << self
        #
        # Imports a .chum5 file uploaded by the user
        #
        # @param [String] file The File Location
        #
        # @return [Character] The Processed Character
        #
        def import_chum5(file)
            # Debug Code
            puts "Importing: #{file.path}"
            # Opening The File
            f=File.open(file.path,'r')
            data=f.read
            f.close
            # Parsing out the data we need from the file
            boop=Nokogiri::XML(data){|config| config.noent}
            c_alias=boop.xpath('.//alias/text()')[0]
            c_name=boop.xpath('.//name/text()')[0]
            mugshots=boop.xpath('.//mugshot/text()').map(&:text)
            boop.xpath('.//mugshots').remove
            # Creating Our shiny new Entry
            character=Character.create! alias: c_alias, name: c_name,  data: nil, images: mugshots
            # Ok we should go ahead and grab the things we need the character constructed for...
            CharacterContact.CharacterContacts_from_chum5_xml(character,boop)
            CharacterAttribute.attributes_from_chum5_xml(character,boop)
            CharacterSkill.skills_from_chum5_xml(character, boop)
            CharacterQuality.qualities_from_chum5_xml(character, boop)
            CharacterItem.items_from_chum5_xml(character,boop)
            boop.xpath('.//contacts').remove
            character.data=boop.to_s
            character.save!

            # More Debug Code
            puts "Imported!"
            puts "Alias: #{c_alias}"
            puts "Name: #{name}"
            puts "Mugshots: #{mugshots.length}"
            puts "Skills: "
        end
        private
        #
        # Turns a Nokogiri node into a JSON string recursively
        #
        # @param [Nokogiri::XML::Node, Nokogiri::XML::Document] node Source node to convert
        #
        # @return [String] JSONified node
        #
        def nokogiri_to_json(node)
            # Well Time for Horror inducing recursive code
            
            # Initial Test case, Are we looking at a document? if so return the first available node, run thru the function.
            return nokogiri_to_json(node.children[0]) if node.class==Nokogiri::XML::Document
            # Checking if it's an empty node
            return nil if node.nil?||(node.class==Nokogiri::XML::Text&&node.to_s.squish=="")
            # Ok Checking for bottom out condition, if we either arrive at a text node, or a empty value
            return node.to_s.dump if node.class==Nokogiri::XML::Text
            # Well We didn't Hit our Bottom out, so let's go ahead and grab our attributes (if any)
            attribute_string=''
            node.attributes.each do |k,v|
                attribute_string+="#{k.dump}:#{v.dump},"
            end
        
            # Let's wrap up our Attributes in a nice little package
            attribute_string="\"attributes\":{#{attribute_string[0..-2]}},"
            # Ok let's get started on doing the recursive bit, the value array
            value_string=""
            # Each Child we are adding a bit of data to our value_string, containing the results of this function ran on a subnode
            # OK there has to be a better way to do this ...
            node.children.each do |subnode|
                json=nokogiri_to_json(subnode)
                value_string+="#{json}, " unless json.nil?
            end
            # Wrapping up our value string in a nice little package
            value_string="\"value\":[#{value_string.gsub(/, $/,'')}]"
            # And Writing Our Node, with a nice little bow on top
            "{\"#{node.name}\":{#{attribute_string}#{value_string}}}"
        end
        #
        # Converts a JSON string to a Nokogiri Node
        #
        # @param [String] json JSON string
        #
        # @return [Nokogiri::XML:Node] Returned Nokogiri Noded
        #
        def json_to_nokogiri(json)
            Nokogiri::XML(json_to_xml(json))
        end
        #
        # Converts a JSON string to an XML Document
        #
        # @param [String] json formated string  
        #
        # @return [String] XML formated String
        #
        def json_to_xml(json)
            json_hash_to_xml(JSON.parse(json))
        end
        #
        # Converts a JSON hash (One that would be created from Nokogiri to JSON) to an XML Document
        #
        # @param [Hash] hash JSON Hash
        #
        # @return [String] XML Formtated String
        #
        def json_hash_to_xml(hash)
            # And Even more horor inducing code
        
            # Checking for Bottom case: Is what we are looking at a string?
            return hash if hash.class==String
            # Ok, Since we didn't hit the Bottom case, Let's go ahead and start out by grabing our keys...
            return_string=''
            hash.each do |tag,value| #bit silly, but makes it easier to grab ALL THE TAGS!
        
                # Ok Grabbing our Attributes
                attribute_string=''
                # Each one of Our attributes ...
                value['attributes'].each do |attribute|
                    # Grab The Attribute's Key and value ...
                    attribute.each do |k,v|
                        # And Shove it in our string!
                        attribute_string+="#{key}=\"#{value}\" "
                    end
                end
        
                # Next up: Let's grab the Child Nodes
                child_node_string=''
                value['values'].each do |child_node|
                    child_node_string+="#{json_hash_to_xml(child_node)}"
                end
                return_string+="<#{tag} #{attribute_string[0..-2]}>#{child_node_string}</#{tag}>"
            end
            return_string
        end
    end
end
