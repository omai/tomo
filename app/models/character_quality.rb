#
# Our Class Container for Character Qualities
#
class CharacterQuality < ApplicationRecord
  belongs_to :quality_listing
  belongs_to :character
  #
  # @!attribute quality_listing
  #   @return [QualityListing] Associated quality listing for the quality in question
  # @!attribute extra
  #  @return [String] Any Extra text associated with our quality
  # @!attribute notes
  #  @return [String] Player notes for the quality
  #
  def name
    return quality_listing.name
  end
  class << self
    #
    # Grabs all Quality nodes from a nokogiri document, and returns an array of CharacterQualities
    #
    # @param [Nokogiri::XML::Document, Nokogiri::XML:Node] nokogiri_document Document/Node containing all of our qualities to be processed
    #
    # @return [Array<CharacterQuality>] Array of Character Qualities from the Nokogiri Document
    #
    def qualities_from_chum5_xml(character, nokogiri_document)
        nokogiri_document.xpath('.//qualities/quality').to_a.map{|quality| quality_from_chum5_xml(character, quality)}
    end
    #
    # Converts a Nokogiri Node into a processed Character Skill
    #
    # @param [Nokogiri::XML:Node] nokogiri_node Node to convert to a Character Quality
    #
    # @return [CharacterQuality] Processed Character Quality
    #
    def quality_from_chum5_xml(character, nokogiri_node)
        id = nokogiri_node.xpath('id/text()').to_s
        quality_listing = QualityListing.find_by_quality_id(id)
        extra = nokogiri_node.xpath('extra/text()').to_s
        notes = nokogiri_node.xpath('notes/text()').to_s
        qual=CharacterQuality.new( quality_listing: quality_listing, extra:extra, notes:notes, character: character )
        qual.save!
        qual
    end
  end
end
