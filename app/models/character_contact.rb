class CharacterContact < ApplicationRecord
  belongs_to :contact
  belongs_to :character
  around_destroy :destroy_character_only_contacts
  def to_chum5_nokogiri()
    contact_nokogiri=contact.to_chum5_nokogiri()
    # Overwriting the placeholder values with the ones for our content
    contact_nokogiri.xpath('.//loyalty').first.content=loyalty
    contact_nokogiri.xpath('.//blackmail').first.content=blackmail
    contact_nokogiri.xpath('.//family').first.content=family
    contact_nokogiri
  end
  def to_chum5_xml()
    to_chum5_nokogiri().to_xml
  end
  class<<self
    def CharacterContacts_from_chum5_xml(character, nokogiri_document)
      # Alot like the one from the contact model ... just itterate over 
      # every contact in the document
      nokogiri_document.xpath('.//contacts/contact').each do |contact|
        CharacterContact_from_chum5_xml(character,contact)
      end
    end
    def CharacterContact_from_chum5_xml(character, nokogiri_node)
      # Ok, let's go ahead and grab a new CharacterContact
      character_contact=CharacterContact.new()
      # Next, let's go ahead Create the character we are going to need
      character_contact.contact=Contact.contact_from_chum5_xml(nokogiri_node)
      # Sweet, let's go ahead and set our character
      character_contact.character=character
      # And we should grab the loyalty for that contact
      character_contact.loyalty=nokogiri_node.xpath('.//loyalty/text()').to_s.to_i
      character_contact.blackmail=nokogiri_node.xpath('.//blackmail/text()').to_s.downcase[0]=='t'
      character_contact.family=nokogiri_node.xpath('.//family/text()').to_s.downcase[0]=='t'
      # Last, but not least let's save this thing!
      character_contact.save!
    end
  end
  private
  def destroy_character_only_contacts()
    c2=contact # Temporarly storing the contact
    yield      # Yielding for Around
    if(c2.player_custom && c2.character_contact.length==0)
      c2.destroy
    end
  end
end
