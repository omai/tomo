#
# Our Class Container for Character Skills
#
class CharacterSkill < ApplicationRecord
  include XMLHelper
  belongs_to :skill_listing
  belongs_to :character
  # @!attribute skill_listing
  #   @return [SkillListing] Associated Skill listing for the skill in question
  # @!attribute character
  #   @return [Character] Character with which the skill is associated with.
  # @!attribute is_knowledge
  #   @return [Boolean] Wether or not the skill is a Knowledge Skill
  # @!attribute specialization
  #   @return [String] Specialization for the Skill in question
  # @!attribute notes
  #   @return [String] Notes for the Skill in question
  # @!attribute karma
  #   @return [Fixnum] Points bought via Karma
  # @!attribute base
  #   @return [Fixnum] Base points bought at character creation
  # @!attribute  name
  #   @return [String] Name for the skill
  # @!attribute  category
  #   @return [String] Category for the skill
  def name
    s_name=read_attribute(:name)
    return skill_listing.name if s_name.nil?
    s_name
  end
  
  def category
    s_catagory=read_attribute(:category)
    return skill_listing.category if s_category.nil?
    s_catagory
  end
  
  #
  # Returns the total value for the skill
  #
  # @return [Fixnum] Total Value for the skill
  #
  def total
    # TODO: Right ... when we do do bonuses we should add it in somewhere in here.
    karma+base
  end
  #
  # Converts the Skill to a Chum5 formated XML string
  #
  # @return [String] Chum5 formated XML String
  #
  def to_chum5_xml()
    Nokogiri::XML::Builder.new do |xml|
        xml.skill{
            xml.guid SecureRandom.uuid
            if ! skill_listing.nil?
                xml.suid skill_listing.skill_id
            else
                # This is how chummer deals with empty nodes
                xml.suid '00000000-0000-0000-0000-000000000000'
            end
            xml.isknowledge is_knowledge
            xml.skillcategory category
            xml.karma karma
            xml.base base
            xml.notes notes
            xml.name name unless name.nil?
            if ! @specialization.nil?
                xml.specs{
                    xml.spec{
                        xml.guid SecureRandom.uuid
                        xml.name specialization
                        xml.free false
                    }
                }
            end
        }
    end.to_xml
  end
  class << self
    #
    # Grabs all the Skill nodes from a Nokogiri Document, and returns an array of Skills
    #
    # @param [Character] Character that should be associated with the new skills
    # @param [Nokogiri::XML::Document, Nokogiri::XML:Node] nokogiri_document Document/Node containing all of our skills to be processed
    #
    # @return [Array<CharacterSkill>] Array of Character Skills from the Nokogiri Document
    #
    def skills_from_chum5_xml(character, nokogiri_document)
        nokogiri_document.xpath('.//skills/skill').to_a.map{|skill| skill_from_chum5_xml(character, skill)}
    end
    #
    # Converts a Nokogiri Node into a processed Character Skill
    #
    # @param [Character] Character that should be associated with the new skill
    # @param [Nokogiri::XML:Node] nokogiri_node Node to convert to a Character Skill
    #
    # @return [CharacterSkill] Processed Character Skill
    #
    def skill_from_chum5_xml(character, nokogiri_node)
      id = nokogiri_node.xpath('suid/text()').to_s
      listing = nil
      name = nil
      category = nil
      type = nil
      puts SkillListing.exists?(skill_id: id)
      if(SkillListing.exists?(skill_id: id))
          listing = SkillListing.find_by(skill_id: id)
          puts "Found Listing!: #{listing.name}"
          # Ok we are assuming we are not going to need name, or catagory then
      else
          # Ok Our skill listing doesn't exist, we need to set our name and our category
          name = nokogiri_node.xpath('name/text()').to_s
          category = nokogiri_node.xpath('skillcategory/text()').to_s
      end
      is_knowledge=nokogiri_node.xpath('isknowledge/text()').to_s.downcase=='t'
      type=nokogiri_node.xpath('type/text()').to_s if is_knowledge
      specialization=nokogiri_node.xpath('specs/spec/name/text()').to_s
      notes=nokogiri_node.xpath('notes/text()').to_s
      karma=nokogiri_node.xpath('karma/text()').to_s.to_i
      base=nokogiri_node.xpath('base/text()').to_s.to_i
      cs = CharacterSkill.new( character: character,
                                skill_listing: listing,
                                name: name,
                                category: category,
                                type: type,
                                is_knowledge: is_knowledge,
                                specialization: specialization,
                                notes: notes,
                                karma: karma,
                                base: base)
      cs.save!
      cs
    end
  end
end
