class Contact < ApplicationRecord
    has_many :character_contact, dependent: :delete_all
    def to_chum5_nokogiri()
        Nokogiri::XML(to_chum5_xml()).children[0]
    end
    def to_chum5_xml()
        Nokogiri::XML::Builder.new do |xml|
            xml.contact{
                xml.name name
                xml.role archetype
                xml.location location
                xml.connection connection
                xml.loyalty 0
                xml.metatype metatype
                xml.sex sex
                xml.age age
                xml.contacttype contact_type
                xml.preferredpayment
                xml.type 'Contact'
                xml.file nil
                xml.relative nil
                xml.notes notes
                xml.groupname nil
                xml.colour -2830136
                xml.group group
                xml.family false
                xml.blackmail false
                xml.free false
                xml.groupenabled false
                # Need to generate a guid or a uuid
                xml.guid SecureRandom.uuid
                xml.mainmugshotindex -1
                xml.mugshots nil
            }
        end.to_xml
    end
    class << self
        def contacts_from_chum5_xml(nokogiri_document)
            nokogiri_document.xpath('.//contacts/contact').each do |contact|
                contact_from_chum5_xml(contact)
            end
        end
        def contact_from_chum5_xml(nokogiri_node)
            #Duplicating the node so we don't lose data.
            node=nokogiri_node.dup
            contact=Contact.new()
            # Grabbing Contact Name
            contact.name= node.xpath('.//name/text()').to_s
            # Grabbing Contact Location
            contact.location= node.xpath('.//location/text()').to_s
            # Grabbing Contact Archetype
            contact.archetype= node.xpath('.//role/text()').to_s
            # Grabbing other data . . . 
            contact.metatype=node.xpath('.//metatype/text()').to_s
            contact.sex=node.xpath('.//sex/text()').to_s
            contact.age=node.xpath('.//age/text()').to_s
            contact.prefered_payment=node.xpath('.//preferredpayment/text()').to_s
            contact.hobbies_vice=node.xpath('.//hobbiesvice/text()').to_s
            contact.personal_life=node.xpath('.//personallife/text()').to_s
            contact.group=node.xpath('.//group/text()').to_s.downcase[0]=='t'
            contact.connection=node.xpath('.//connection/text()').to_s.to_i
            contact.contact_type=node.xpath('.//contacttype/text()').to_s
            # Grabbing Notes!
            contact.notes=node.xpath('.//notes/text()').to_s
            # Purging Loyalty
             node.xpath('.//loyalty').remove
            # Saving Data
            contact.data= node.to_s
            # Last Step: Setting Visible, and player_custom to false and true respectively
            contact.visible=false;
            contact.player_custom=true;
            contact.save!
            # And Just to make our lives easier ... we are going to return the contact.
            contact
        end
    end
end
