class QualityListing < ApplicationRecord
    include ListingHelper
    #
    # @!attribute quality_id
    #   @return [String] ID of the quality in qualties.xml
    # @!attribute name
    #  @return [String] Name of the quality
    # @!attribute karma
    #  @return [Fixnum] Karma Cost/Bonus for the Quality
    # @!attribute category
    #  @return [String] Category for the skill
    # @!attribute limit
    #  @return [Integer] Number of times the Quality can be taken. If none is given it will default to nil.
    # @!attribute bonus
    #  @return [Array<String>] Bonuses that the quality will give the character (Should convert this to a bonus class, but for right now expect XML strings)
    # @!attribute forbidden
    #  @return [QualityListing::ListingList] Anything that is forbidden to be "mixed" with this quality
    # @!attribute required
    #  @return [QualityListing::ListingList] Anything that is required to take this quality
    # @!attribute only_priority_given
    #  @return [Boolean] Whether or not the quality should be only available if the priority is set up appropriatly
    # @!attribute name_on_page
    #  @return [String] The name of the quality on the page (if somehow different than the one in the book)
    # @!attribute cost_discount
    #  @return [QualityListing::CostDiscount] Discount for the quality.
    # @!attribute source
    #  @return [String] Source book for the quality in question
    # @!attribute page
    #  @return [String] Page for the quality in question
    #
    # TODO: Add Metagenetic, contributetolimit, doublecareer, mutant, first level bonus to this listing.
    serialize :bonus, Array
    # serialize :forbidden, QualityListing::ListingList
    # serialize :required, QualityListing::ListingList
    # serialize :cost_discount, CostDiscount
    class << self
        # Loads All Quality listings for a version of Chummer
        #
        # @param [String] version_number Version for the listings
        #
        def load_listings(version_number)
            xml_data=ListingHelper.get_listing(version_number,'qualities.xml')
            xml_data.xpath('.//qualities/quality').each do |quality|
                # Grabbing our quality_id
                quality_id=quality.xpath('id/text()').to_s
                # Welp this is looking familiar, checking to see if the quality is in the database already
                quality_listing=QualityListing.find_or_create_by(quality_id: quality_id)
                # And overwriting some crap ... again
                quality_listing.name=quality.xpath('name/text()').to_s
                quality_listing.karma=quality.xpath('karma/text()').to_s.to_i
                quality_listing.category=quality.xpath('category/text()').to_s
                limit=quality.xpath('limit/text()').to_s.to_i
                quality_listing.limit=nil
                quality_listing.limit=limit if limit>0
                # TODO: Write code here when Bonuses are finished (gods help me that's gonna be a fucking bitch and a half.)
                quality_listing.bonus=quality.xpath('bonus').to_a.map{|b| b.to_xml}
                # Insert Snappy code for forbidden, required, etc
                # Ok, this is just checking to see if we get a node back, if we do we know to set this flag to true.
                quality_listing.only_priority_given=!quality.xpath('onlyprioritygiven').empty?
                quality_listing.name_on_page=quality.xpath('nameonpage/text()').to_s
                # We are setting these to nil for right now, we will be back around for a part two to be executed afterwards
                quality_listing.cost_discount=nil
                quality_listing.forbidden=nil
                quality_listing.required=nil
                quality_listing.source=quality.xpath('source/text()').to_s
                quality_listing.page=quality.xpath('page/text()').to_s
                quality_listing.save!
            end
        end
        def load_restrictions(version_number)
            # Boilerplate for loading our listing...
            xml_data=GetListingNokogiri.get_listing(version_number,'qualities.xml')
            xml_data.xpath('.//qualities/quality/forbidden|required|costdiscount').to_a.map{|a| a.parent}.each do |quality|
                # Grabbing our quality_id
                quality_id=quality.xpath('id/text()').to_s
                # Welp this is looking familiar, checking to see if the quality is in the database already
                quality_listing=QualityListing.find_or_create_by(quality_id: quality_id)
                quality_listing.cost_discount=CostDiscount.from_listing(quality)
                quality_listing.forbidden=ListingList.from_listing(quality)
                quality_listing.required=ListingList.from_listing(quality)
                quality_listing.save!
            end
        end
    end
    class ListingList
        #
        # @!attribute all_of - Sets the requirement for All of the following items in the array(Should be a skill listing/quality/item followed by the index for the item (Can be set to a string if the Class for the item doesn't exist yet...))
        #  @return [Array<Array<Class, Fixnum, String>>] 
        # @!attribute one_of - Sets the requiremet for One of the following items in the array (Should be a skill listing/quality/item followed by the index for the item (Can be set to a string if the Class for the item doesn't exist yet...))
        #  @return [Array<Array<Class, Fixnum, String>>]
        #
        attr_accessor :all_of, :one_of
        #
        # Initializes our ListingList Class
        #
        # @param [Array<Array<Class, Fixnum, String>>] one_of Any node that is a requirement, see the attribute listing for more detail!
        # @param [Array<Array<Class, Fixnum, String>>] all_of All nodes that are an absolute requirement, see the attribute listing for more detail.
        #
        def initialize(options={all_of:[], one_of:[]})
            @all_of=options[:all_of]
            @one_of=options[:one_of]
        end
        class << self
            #
            # Initializes an instance of this class from a nokogiri node.
            #
            # @param [Nokogiri::XML::Node] nokogiri_node The Node we are initializing from
            #
            # @return [QualityListing::ListingList] The intialized class
            #
            def from_listing(nokogiri_node)
                # TODO: actually write code here when Item listing is finished.
                ''
            end
        end
    end
    class CostDiscount < ListingList
        #
        # @!attribute discount_ammount
        #  @return [Fixnum] Ammount the quality should be discounted by
        # @!attribute associations
        #  @return [Array<Class,Fixnum>]
        attr_accessor :discount_ammount
        def initialize(options={discount_ammount: 0})
            @discount_ammount=options[:discount_ammount]
            super(options)
        end
        class << self
            #
            # Initializes an instance of this class from a nokogiri node.
            #
            # @param [Nokogiri::XML::Node] nokogiri_node The Node we are initializing from
            #
            # @return [QualityListing::CostDiscount] The intialized class
            #
            def from_listing(nokogiri_node)
                # TODO: actually write code here when Item Listing is finished.
                super(nokogiri_node)
            end
        end
    end 
end
