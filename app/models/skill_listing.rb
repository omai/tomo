class SkillListing < ApplicationRecord
    serialize :specializations, Array
    class << self
        # Loads All Skill listings for a version of Chummer
        #
        # @param [String] version_number Version for the listings
        #
        def load_listings(version_number)
            xml_data=ListingHelper.get_listing(version_number,'skills.xml')
            # Alright now that we have the data loded in, let's start writing this shit!
            xml_data.xpath('.//skill').each do |skill|
                # Grabbing our skill_id
                skill_id=skill.xpath('id/text()').to_s
                # Sick, now then let's check to see if the skill in the database already
                skill_listing=SkillListing.find_or_create_by(skill_id: skill_id)
                # Well regardless ... let's go ahead and overwrite those entries
                skill_listing.name=skill.xpath('name/text()').to_s
                skill_listing.skill_attribute=skill.xpath('attribute/text()').to_s
                skill_listing.category=skill.xpath('category/text()').to_s
                skill_listing.defaultable=skill.xpath('default/text()').to_s()[0].downcase=='t'
                skill_listing.skill_group=skill.xpath('skillgroup/text()').to_s
                skill_listing.specializations=skill.xpath('specs/spec').map{|spec| spec=spec.content.to_s}
                skill_listing.source=skill.xpath('source/text()')
                # And let's go ahead and save them!
                skill_listing.save!
            end
        end
    end
end
