class GearListing < ApplicationRecord
    include ListingHelper
    include StringCalc
    #
    # @!attribute gear_id
    #   @return [String] ID for the gear in gear.xml
    # @!attribute name
    #   @return [String] Name for the gear
    # @!attribute category
    #   @return [String] Category for the gear
    # @!attribute rating
    #   @return [Fixnum] Rating for the gear, if any
    # @!attribute source
    #   @return [String] Source book In which the gear is listed
    # @!attribute page
    #   @return [String] Page number that the gear is on in it's source book
    # @!attribute availibility
    #   @return [String] Availability for the gear
    # @!attribute cost
    #   @return [String] Cost of the gear / Calculation to figure out the cost of the gear (in Nuyen)
    # @!attribute cost_for
    #   @return [Fixnum] Total number of the gear to be given when the cost is payed
    # @!attribute capacity
    #   @return [String] Capacity the item has, or consumes for mods (defaults to 0)
    # @!attribute armor_capacity
    #   @return [String] Capacity the item has, or consumes for Armor Mods (Defaults to 0)
    # TODO: Write weapon capacity? idk
    # @!attribute bonus
    #   @return [String] XML String for bonuses (will be changed to it's own class later)
    # @!attribute weapon_bonus
    #   @return [String] XML String for bonuses (Will be turned into it's own class later, maybe need to write a weapon class in particular?)
    # @!attribute require_parent
    #   @return [Boolean] Whether we should require a parent for the item or not
    # @!attribute min_rating
    #   @return [Fixnum] Minimum rating tha tthe item allowed to have
    # @!attribute add_on_categories
    #   @return [Array<String>] Any aditional catagories that we should have other than our primary.
    # @!attribute allow_rename
    #   @return [Boolean] should we allow the user to rename the item
    # TODO: Make a boolean method for this.
    # @!attribute can_form_persona
    #   @return [String] can the item form a persona, if so where
    # @!attribute device_rating
    #   @return [String] Formula for calculating the device rating, or the device rating itself
    # @!attribute matrix_condition_monitor_bonus
    #   @return [Integer] How much we should modify the parent's condition modifier (Defaults to 0)
    # @!attribute programs
    #   @return [String] either the string for calculating the number of programs, or the number of programs itself
    # @!attribute attribute_array
    #  @return [String] Default attribute array (usually doesn't exist, except for ciberdecks)
    # @!attribute attack
    #   @return [String] String for calculating the attack rating of the item, or the attack rating overall
    # @!attribute sleaze
    #   @return [String] String for calculating the sleaze rating of the item, or the sleaze rating overall
    # @!attribute data_processing
    #   @return [String] String for calculating the data processing rating of the item, or the data processing rating overall
    # @!attribute firewall
    #   @return [String] String for calculating the firewall rating of the item, or the firewall rating overall
    # @!attribute mod_attack
    #  @return [String] String for calculating the attack modifier of the item or the attack modifier overall
    # @!attribute mod_sleaze
    #  @return [String] String for calculating the sleaze modifier of the item or the sleaze modifier overall
    # @!attribute mod_data_processing
    #  @return [String] String for calculating the data processing modifier of the item or the data processing modifier overall
    # @!attribute mod_firewall
    #  @return [String] String for calculating the firewall modifier of the item or the firewall modifier overall
    # @!attribute mod_attribute_array
    #   @return [String] String for modifing the attribute array
    # @!attribute sub_gears
    #   @return [Array<gear_listing>] List of subgear that should be automaticly given if a character gets this item.
    #
    #
    #
    # Calculates the availibilty of the item based off of a set of variables
    #
    # @param [Hash<String,Fixnum>] variables Hash of variables that we are going to replace before calculation
    #
    # @return [String] Calculated Availibility Code
    #
    serialize :add_on_categories, Array
    #
    # Check to see if the listing allows the attributes to be swapped
    #
    # @return [Boolean] Whether the attributes for a connected device can be swapped
    #
    def can_swap_attributes?
        attribute_array.empty?
    end
    #
    # Calculates the availability based off of a few user provided attributes
    #
    # @param [Hash<String, Fixnum>] variables Variables to be replaced
    #
    # @return [String] Compiled availability
    #
    def calculate_availability(variables={'Rating' => 1})
        # Ok just really quick, let's go ahead and send our availability through calculate Fixed Prices to just ensure that's not what's going on.
        a=StringCalc.calculate_fixed_values(availability, variables['Rating'])
        # Quick regex to seperate out availibility formula from licence code (if any)
        match=/^(.+)([FR])$/.match(a)
        # Some varaibles for later ... 
        avail_formula=''
        license_code=''
        # CChecking if we do need to seperate our licence code, like a yolk from an egg
        if(match.nil?)
            avail_formula=a
        else
            avail_formula=match[1]
            license_code=match[2]
        end
        avail=StringCalc.calculate_formula_with_variables(avail_formula, variables)
        avail.to_s+license_code
    end
    #
    # Calculates the cost of the item based of a set of varaibles
    #
    # @param [Hash<String,Fixnum>] varaibles Hash of varaibles that we are going to replace before calculation
    #
    # @return [String] Calculated Cost (In Nuyen)
    #
    def calculate_cost(varaibles={'Rating' => 1})
        # Ok before attempting any formula weirdness, let's run it through calulating the fixed values first, to see if there is anything there...
        c=StringCalc.calculate_fixed_values(cost, varaibles['Rating'])
        # And then let's send that result to calculate formula
        StringCalc.calculate_formula_with_variables(c, varaibles).to_s
    end
    class << self
        #
        # Loads All Gear listings for a version of Chummer
        #
        # @param [String] version_number Version for the listings
        #
        def load_listings(version_number)
            xml_data=ListingHelper.get_listing(version_number,'gear.xml')
            xml_data.xpath('.//gears/gear').each do |gear|
                gear_id=gear.xpath('id/text()').to_s
                gear_listing=GearListing.find_or_create_by(gear_id: gear_id)
                gear_listing.name=gear.xpath('name/text()').to_s
                gear_listing.category=gear.xpath('category/text()').to_s
                gear_listing.rating=gear.xpath('rating/text()').to_s.to_i
                gear_listing.source=gear.xpath('source/text()').to_s
                gear_listing.page=gear.xpath('page/text()').to_s
                gear_listing.availibility=gear.xpath('avail/text()').to_s
                gear_listing.cost=gear.xpath('cost/text()').to_s
                gear_listing.cost_for=gear.xpath('costfor/text()').to_s.to_i
                gear_listing.cost_for=1 if gear_listing.cost_for==0
                
                gear_listing.capacity=gear.xpath('capacity/text()').to_s
                gear_listing.armor_capacity=gear.xpath('armorcapacity/text()').to_s
                # TODO: add code to calculate bonuses
                gear_listing.bonus=gear.xpath('bonus').to_xml.to_s
                gear_listing.weapon_bonus=gear.xpath('weaponbonus').to_xml
                gear_listing.require_parent=!gear.xpath('requireparent').empty?
                gear_listing.min_rating=gear.xpath('minrating').to_s.to_i
                gear_listing.add_on_categories=gear.xpath('addoncategory').to_a.map{|aoc|aoc.xpath('text()').to_s}
                allow_rename=gear.xpath('allowrename/text()').to_s
                if(allow_rename.nil?)
                    gear_listing.allow_rename=false
                else
                    gear_listing.allow_rename=allow_rename[0]=='T'
                end
                gear_listing.can_form_persona=gear.xpath('canformpersona/text()').to_s
                gear_listing.device_rating=gear.xpath('devicerating/text()').to_s
                gear_listing.matrix_condition_monitor_bonus=gear.xpath('matrixcmbonus/text()').to_s.to_i
                gear_listing.programs=gear.xpath('programs/text()').to_s
                gear_listing.attribute_array=gear.xpath('attributearray/text()').to_s
                gear_listing.attack=gear.xpath('attack/text()').to_s
                gear_listing.sleaze=gear.xpath('sleaze/text()').to_s
                gear_listing.data_processing=gear.xpath('dataprocessing/text()').to_s
                gear_listing.firewall=gear.xpath('firewall/text()').to_s
                gear_listing.mod_attack=gear.xpath('modattack/text()').to_s
                gear_listing.mod_sleaze=gear.xpath('modsleaze/text()').to_s
                gear_listing.mod_data_processing=gear.xpath('moddataprocessing/text()').to_s
                gear_listing.mod_firewall=gear.xpath('modfirewall/text()').to_s
                gear_listing.mod_attribute_array=gear.xpath('modattributearray/text()').to_s
                # TODO: Convert this from a database column to a simple check.
                gear_listing.save!
            end
            # Ok, welp oh boy oh boy, next up let's go ahead and grab our list of potential sub gears
        end
    end
    # TODO: Add code to calculate variable prices, and Distributed Decks (Oh gods why?!)
    

end
