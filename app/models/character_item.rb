class CharacterItem < ApplicationRecord
  include StringCalc
  include XMLHelper
  belongs_to :gear_listing
  belongs_to :character
  belongs_to :parent_item, class_name: 'CharacterItem', foreign_key: 'parent_item', optional: true
  # @!attribute gear_listing
  #   @return [GearListing] Listing that this item is based on
  # @!attribute character
  #   @return [Character] Character that this item belongs to
  # @!attribute parent_item
  #   @return [CharacterItem] Item that is this item's parent (if any)
  # @!attribute rating
  #   @return [Fixnum] Rating for the gear(if not default)
  # @!attribute quantity
  #   @return [Fixnum] Number of items in this stack
  # @!attribute availability
  #   @return [String] Availability of this item in question
  # @!attribute cost
  #   @return [String] Value of the item (in Nuyen)
  # @!attribute bonded
  #   @return [Boolean] If the item is bonded to the character
  # @!attribute equipped
  #   @return [Boolean] If the item is equipped
  # @!attribute wireless_on
  #   @return [Boolean] If the item has it's wireless functionality (if any) on
  # @!attribute matrix_condition_monitor_filled
  #   @return [Fixnum] How many boxes of matrix damage the item has recieved
  # @!attribute notes
  #   @retrun [String] Any notes that are associated with the item.
  # @!attribute capacity
  #   @return [Fixnum] Capacity the item has for mods, or takes up 
  # @!attribute armor_capacity
  #   @return [Fixnum] Capacity the item takes up in a piece of armor as a mod
  # @!attribute min_rating
  #   @return [Fixnum] Minimum rating for an item
  # @!attribute extra
  #   @return [String] Any extra text for a item (coudl be used to determine what the item is compatable with (ie: Heavy Pistols to indicate ammuniton is for that type of weapon), or as descriptive text (ie: Indicating Country of orgin/name for a fake SIN)
  # @!attribute bonus
  #   @return [String] Bonus XML String for calculating a bonus (note: this is going to change later to it's own class)
  # @!attribute wireless_bonus
  #   @return [String] Bonus XML String for calculating a bonus for when the device has it's wireless capabilities turned on (note: this is going to change later to it's own class)
  # @!attribute armor_bonus
  #   @return [String] Bonus XML string for calculating a bonus for when the device is part of a piece of armor
  # @!attribute can_form_persona
  #   @return [Boolean] Whether or not the item can form a persona on the matrix
  # FIXME: I'm not sure if this needs to be changed to a string or not, but labeling it as such rn. need to test with a technomancer
  # @!attribute device_rating
  #   @return [Fixnum] The device rating for the item
  # TODO: Figure out what the fuck forced_value/matrix_condition_monitor_bonus is for here.
  # @!attribute forced_value
  #   @return [String] The forced value for an item???? I honestly don't know what this is for yet.
  # @!attribute matrix_condition_monitor_bonus
  #   @return [Fixnum] Another unknown, should figure out what this is for
  # @!attribute allow_rename
  #   @return [Boolean] Whether the user can define a custom name for the item
  # @!attribute location
  #   @return [String] Where the item is stored
  # @!attribute discounted_cost
  #   @return [Boolean] Whether the item was bought at a discounted rate
  # TODO: figure out what program_limit is for.
  # @!attribute program_limit
  #   @return [String] And yet another unknown. I'm assuming that this is for how many programs a deck can hold?
  # @!attribute overclocked
  #   @return [Boolean] Whether the device is currently overclocked (I think this has to do with the technoadept complex form)
  # TODO: convert this to an array of strings/fixnums, whichever makes the most sense
  # @!attribute attribute_array
  #   @return [String] Attribute array available to the user to play around with attack/sleaze/data_processing/firewall (having this enabled indicates that they can be switched)
  # @!attribute attack
  #   @return [String] Formula to calculate the devices attack rating in the matrix
  # @!attribute sleaze
  #   @return [String] Formula to calculate the devices Sleaze rating in the matrix
  # @!attribute data_processing
  #   @return [String] Formula to calculate the devices data_processing rating in the matrix
  # @!attribute firewall
  #   @return [String] Formula to calculate the devices firewall rating in the matrix
  # @!attribute mod_attack
  #   @return [String] Add on formula to add to calculating the parents attack rating
  # @!attribute mod_sleaze
  #   @return [String] Add on formula to add to calculating the parents sleaze rating
  # @!attribute mod_data_processing
  #   @return [String] Add on formula to add to calculating the parents data_processing rating
  # @!attribute mod_firewall
  #   @return [String] Add on formula to add to calculating the parents firewall rating
  # TODO: convert this to an array of strings/fixnums, whichever makes the most sense
  # @!attribute mod_attribute_array
  #   @return [String] Bonus that should be added to the attribute array
  # FIXME: Half way certian that we can just determine this form whether or not there is an attribute array, instead of having a seperate flag in the database for this
  # @!attribute can_swap_attributes
  #   @return [Boolean] Whether or not we can swap attributes for this matrix device
  # @!attribute active
  #   @return [Boolean] Whether or not the item is currently active
  # TODO: Determine if this is accurate
  # @!attribute home_node
  #   @return [Boolean] Whether or not the device is the home node for an AI
  # @!attribute sort_order
  #   @return [Fixnum] Modifier for sort order for the item.
  
  #
  # Returns the name with the extra text if applicable
  #
  # @return [String] 
  #
  def name_with_extra
    returner=name
    returner += " (#{extra})" unless extra.empty?
    returner
  end
  #
  # Gets all child items associated with a parent item
  #
  # @return [Array<CharacterItem>] All child items for the item.
  #
  def child_items
    CharacterItem.where("parent_item = #{id}")
  end
  #
  # Whether or not the item has a parent or not (Alias for ! parent_item.nil?)
  #
  # @return [Boolean] if the item has a parent
  #
  def has_parent?
    ! parent_item.nil?
  end
  # TODO: add special formulas here to calculate deck ratings for both technos and deckers
  
  #
  # Calculates the Total Attack for the matrix item
  #
  # @return [Fixnum] Attack rating for the matrix item
  #
  def calc_attack
    raise NotImplementedError
  end
  
  #
  # Calculates the Sleaze value for the matrix item
  #
  # @return [Fixnum] Sleaze rating for the matrix item
  #
  def calc_sleaze
    raise NotImplementedError
  end
  
  #
  # Calculates the Data Processing value for the matrix Item
  #
  # @return [Fixnum] Data Processing Value for matrix item
  #
  def calc_data_processing
    raise NotImplementedError
  end
  
  #
  # Calculates the Firewall value for the matrix item
  #
  # @return [Fixnum] Firewall value for the matrix item
  #
  def calc_firewall
    raise NotImplementedError
  end
  class << self
    #
    # Grabs all Gear nodes, and returns an array of processed Character Items
    #
    # @param [Character] character Character whom we are assigning the items
    # @param [Nokogiri::XML::Document, Nokogiri::XML::Node] nokogiri_document Document/Node containing all of our qualities to be processed
    #
    # @return [Array<CharacterItem>] Array of Character items from the document
    #
    def items_from_chum5_xml(character, nokogiri_document)
      nokogiri_document.xpath('.//gears/gear').to_a.map{|gear| item_from_chum5_xml(character, gear)}
    end
    #
    # Converts a Nokogiri Node into a processed Character Item
    #
    # @param [Character] character Character that we are binding this item too
    # @param [Nokogiri::XML:Node] nokogiri_node Node to convert to a Character Item (Recursively)
    # @param [CharacterItem] parent_node Item that is supposed to be the character's parent (Nil unless a child node)
    #
    # @return [CharacterItem] Processed Character Item
    #
    def item_from_chum5_xml(character, nokogiri_node, parent_node=nil)
      gear_id = XMLHelper.grab_string_from_nokogiri(nokogiri_node, 'id/text()')
      gear_listing = GearListing.find_by_gear_id(gear_id)
      parent_item = parent_node
      name = XMLHelper.grab_string_from_nokogiri(nokogiri_node, 'name/text()')
      rating = XMLHelper.grab_int_from_nokogiri(nokogiri_node, 'rating/text()', 1)
      quantity = XMLHelper.grab_int_from_nokogiri(nokogiri_node, 'qty/text()')
      availibility = XMLHelper.grab_string_from_nokogiri(nokogiri_node, 'avail/text()')
      cost = XMLHelper.grab_string_from_nokogiri(nokogiri_node, 'cost/text()')
      bonded = XMLHelper.grab_bool_from_nokogiri(nokogiri_node, 'bonded/text()')
      equipped = XMLHelper.grab_bool_from_nokogiri(nokogiri_node, 'equipped/text()')
      wireless_on = XMLHelper.grab_bool_from_nokogiri(nokogiri_node, 'wirelesson/text()')
      matrix_condition_monitor_filled = XMLHelper.grab_int_from_nokogiri(nokogiri_node, 'matrixcmfilled/text()')
      notes = XMLHelper.grab_string_from_nokogiri(nokogiri_node, 'notes/text()')
      #oh gods we need mooooooaaaarrrrr
      capacity = XMLHelper.grab_string_from_nokogiri(nokogiri_node, 'capacity/text()')
      armor_capacity = XMLHelper.grab_int_from_nokogiri(nokogiri_node, 'armorcapacity/text()') # Might cause a few issues check this first if we start getting errors
      min_rating = XMLHelper.grab_int_from_nokogiri(nokogiri_node, 'minrating/text()')
      extra = XMLHelper.grab_string_from_nokogiri(nokogiri_node, 'extra/text()')
      bonus = nokogiri_node.xpath('bonus').to_xml.to_s
      wireless_bonus = nokogiri_node.xpath('wirelessbonus').to_xml.to_s
      armor_bonus = nil # Uh ... idk?
      can_form_persona = XMLHelper.grab_string_from_nokogiri(nokogiri_node, 'canformpersona/text()')
      device_rating = XMLHelper.grab_int_from_nokogiri(nokogiri_node, 'devicerating/text()')
      forced_value = XMLHelper.grab_string_from_nokogiri(nokogiri_node, 'forcedvalue/text()')
      matrix_condition_monitor_bonus = XMLHelper.grab_int_from_nokogiri(nokogiri_node, 'matrixcmbonus/text()')
      allow_rename = XMLHelper.grab_bool_from_nokogiri(nokogiri_node,'allowrename/text()')
      location = XMLHelper.grab_string_from_nokogiri(nokogiri_node, 'location/text()')
      discounted_cost = XMLHelper.grab_bool_from_nokogiri(nokogiri_node,'discountedcost/text()')
      program_limit = XMLHelper.grab_string_from_nokogiri(nokogiri_node, 'programlimit/text()')
      overclocked = XMLHelper.grab_bool_from_nokogiri(nokogiri_node,'overclocked/text()')
      attribute_array = XMLHelper.grab_string_from_nokogiri(nokogiri_node, 'attributearray/text()')
      attack = XMLHelper.grab_string_from_nokogiri(nokogiri_node, 'attack/text()')
      sleaze = XMLHelper.grab_string_from_nokogiri(nokogiri_node, 'sleaze/text()')
      data_processing = XMLHelper.grab_string_from_nokogiri(nokogiri_node, 'data_processing/text()')
      firewall = XMLHelper.grab_string_from_nokogiri(nokogiri_node, 'firewall/text()')
      mod_attack = XMLHelper.grab_string_from_nokogiri(nokogiri_node, 'modattack/text()')
      mod_sleaze = XMLHelper.grab_string_from_nokogiri(nokogiri_node, 'modsleaze/text()')
      mod_data_processing = XMLHelper.grab_string_from_nokogiri(nokogiri_node, 'moddataprocessing/text()')
      mod_firewall = XMLHelper.grab_string_from_nokogiri(nokogiri_node, 'modfirewall/text()')
      mod_attribute_array = XMLHelper.grab_string_from_nokogiri(nokogiri_node, 'modattributearray/text()')
      can_swap_attributes = XMLHelper.grab_string_from_nokogiri(nokogiri_node, 'canswapattributes/text()')
      active = XMLHelper.grab_bool_from_nokogiri(nokogiri_node,'active/text()')
      home_node = XMLHelper.grab_bool_from_nokogiri(nokogiri_node,'homenode/text()')
      sort_order = XMLHelper.grab_int_from_nokogiri(nokogiri_node, 'sortorder/text()')
      # Ok, next up let's go ahead and save this node (we are going to need this in a moment)
      ci=CharacterItem.new gear_listing: gear_listing, parent_item: parent_node, name: name,
                           rating: rating, quantity:quantity, availability: availibility, cost: cost,
                           bonded: bonded, equipped: equipped, wireless_on: wireless_on, 
                           matrix_condition_monitor_filled: matrix_condition_monitor_filled, 
                           notes: notes, character: character, capacity: capacity, 
                           armor_capacity: armor_capacity, min_rating: min_rating, extra: extra, 
                           bonus: bonus, wireless_bonus: wireless_bonus, armor_bonus: armor_bonus, 
                           can_form_persona: can_form_persona, device_rating: device_rating, 
                           forced_value: forced_value, 
                           matrix_condition_monitor_bonus: matrix_condition_monitor_bonus, 
                           allow_rename: allow_rename, location: location, 
                           discounted_cost: discounted_cost, program_limit: program_limit,
                           overclocked: overclocked, attribute_array: attribute_array, attack: attack,
                           sleaze: sleaze, data_processing: data_processing, firewall: firewall,
                           mod_attack: mod_attack, mod_sleaze: mod_sleaze, 
                           mod_data_processing: mod_data_processing, mod_firewall: mod_firewall,
                           mod_attribute_array: mod_attribute_array, can_swap_attributes: can_swap_attributes,
                           active: active, home_node: home_node, sort_order: sort_order
      ci.save!
      # Ok next and final bit, we need ALL THE CHILD NODES
      nokogiri_node.xpath('children/gear').to_a.each do |child|
        item_from_chum5_xml(character, child, ci)
      end
      # Finally let's go ahead and return all of our nodes
      ci
    end
  end
end
