#
# Class Container for Character Attributes
#
class CharacterAttribute < ApplicationRecord
  belongs_to :character
  # @!attribute name
  #   @return [String] Name of the Attribute in question
  # @!attribute metatype_min
  #   @return [Fixnum] Minimum Score for the Metatype
  # @!attribute metatype_max
  #   @return [Fixnum] Maximum Points for the Metatype
  # @!attribute metatype_aug_max
  #   @return [Fixnum] Maximum points plus maximum points for metatype
  # @!attribute base
  #   @return [Fixnum] Base Number of points assigned at Character creation
  # @!attribute karma
  #   @return [Fixnum] Total number of points bought by Karma
  # @!attribute metatype_category
  #   @return [String] Metatype Catagory? uh ... I'm not 100% sure what this is ... 
  #
  # Returns the total value for this Attribute
  #
  # @return [Fixnum] Total Value for the Attribute
  #
  def total()
    # Our total is the minimum for the metatype + whatever we spent on it during character creation + any aditional karma spent
    metatype_min+base+karma
  end
  class << self
    #
    # Grabs all Quality nodes from a nokogiri document, and returns an array of CharacterAttributes
    #
    # @param [Character] character Character we are associating with all nodes we are creating
    # @param [Nokogiri::XML::Document, Nokogiri::XML:Node] nokogiri_document Document/Node containing all of our Attributes to be processed
    #
    # @return [Array<CharacterAttribute>] Array of Character Qualities from the Nokogiri Document
    #
    def attributes_from_chum5_xml(character, nokogiri_document)
      nokogiri_document.xpath('.//attributes/attribute').to_a.map{|attrib| attribute_from_chum5_xml(character,attrib)}
    end
    #
    # Converts a Nokogiri Node into a processed Character Skill
    #
    # @param [Character] character Character we are associating with this node
    # @param [Nokogiri::XML:Node] nokogiri_node Node to convert to a Character Attribute
    #
    # @return [CharacterQuality] Processed Character Attribute
    #
    def attribute_from_chum5_xml(character, nokogiri_node)
      name=nokogiri_node.xpath('name/text()').to_s
      # Literally fetching all the values for attributes
      metatypemin=nokogiri_node.xpath('metatypemin/text()').to_s.to_i
      metatypemax=nokogiri_node.xpath('metatypemax/text()').to_s.to_i
      metatypeaugmax=nokogiri_node.xpath('metatypeaugmax/text()').to_s.to_i
      base=nokogiri_node.xpath('base/text()').to_s.to_i
      karma=nokogiri_node.xpath('karma/text()').to_s.to_i
      metatypecategory=nokogiri_node.xpath('metatypecategory/text()').to_s
      # Ok time for fun, let's go ahead and well ... create our entry in our Hash.
      c_attribute = CharacterAttribute.new( character: character,
                                            name: name,
                                            metatype_min: metatypemin,
                                            metatype_max: metatypemax,
                                            metatype_aug_max: metatypeaugmax,
                                            base: base,
                                            karma: karma,
                                            metatype_category:metatypecategory)
      c_attribute.save!
      c_attribute
    end
  end
end
