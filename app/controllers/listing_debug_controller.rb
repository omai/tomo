class ListingDebugController < ApplicationController
  def skills
    @available_skills=SkillListing.all
  end
  def qualities
    @available_qualities=QualityListing.all
  end
  def items
    @available_items=GearListing.all
  end
end
