class CharacterContactsController < ApplicationController
  before_action :set_character_contact, only: [:show, :edit, :update, :destroy]

  # GET /character_contacts
  # GET /character_contacts.json
  def index
    @character_contacts = CharacterContact.all
  end

  # GET /character_contacts/1
  # GET /character_contacts/1.json
  def show
  end

  # GET /character_contacts/new
  def new
    @character_contact = CharacterContact.new
  end

  # GET /character_contacts/1/edit
  def edit
  end

  # POST /character_contacts
  # POST /character_contacts.json
  def create
    @character_contact = CharacterContact.new(character_contact_params)

    respond_to do |format|
      if @character_contact.save
        format.html { redirect_to @character_contact, notice: 'Character contact was successfully created.' }
        format.json { render :show, status: :created, location: @character_contact }
      else
        format.html { render :new }
        format.json { render json: @character_contact.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /character_contacts/1
  # PATCH/PUT /character_contacts/1.json
  def update
    respond_to do |format|
      if @character_contact.update(character_contact_params)
        format.html { redirect_to @character_contact, notice: 'Character contact was successfully updated.' }
        format.json { render :show, status: :ok, location: @character_contact }
      else
        format.html { render :edit }
        format.json { render json: @character_contact.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /character_contacts/1
  # DELETE /character_contacts/1.json
  def destroy
    @character_contact.destroy
    respond_to do |format|
      format.html { redirect_to character_contacts_url, notice: 'Character contact was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_character_contact
      @character_contact = CharacterContact.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def character_contact_params
      params.require(:character_contact).permit(:contact_id, :character_id, :loyalty)
    end
end
