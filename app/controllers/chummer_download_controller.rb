class ChummerDownloadController < ApplicationController
    def index
    end
    def download
        puts params.class
        download_listings(params['tag'])
        redirect_to action: 'index'
    end
    def download_listings(tag)
        # Well this isn't the most standard thing to do, but it's so we don't have to refetch, and redo a bunch of listings

        # First thing First, Let's go ahead and grab a directory listing
        available_files=Octokit.contents('chummer5a/chummer5a', path:'Chummer/data', ref: tag).map{|data|data=data[:path]}
        # Annnnnnnnnnnd Let's go ahead and make our shiney new directory for these files
        FileUtils.mkdir_p "vendor/chummer5-listings/#{tag}"
        # Now that we have a listing of Our directories, let's go ahead and fetch all those files, and write them to their files!
        available_files.each do |file| 
            #file_contents=Base64.decode64(Octokit.contents('chummer5a/chummer5a', path:file, ref: tag))
            file_path_local=file.match(/\/.*\/(.*\..*$)/)[1]
            File.open(File.join("#{Dir.pwd}/vendor/chummer5-listings/#{tag}",file_path_local), 'wb') do |f|
                f<<Net::HTTP.get(URI("https://raw.githubusercontent.com/chummer5a/chummer5a/#{tag}/#{file}"))
            end
        end
        # Ok, we are gonna go ahead and load the skill listings next, because we have these down sooooooo 
        SkillListing.load_listings(tag)
        QualityListing.load_listings(tag)
        GearListing.load_listings(tag)
        flash[:notice]="Successfully downloaded Data for Chummer #{tag}"

    # rescue Exception => msg
    #     flash[:notice]="Failed to download Files!\n#{msg}"
    end
end
