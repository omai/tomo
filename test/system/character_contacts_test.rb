require "application_system_test_case"

class CharacterContactsTest < ApplicationSystemTestCase
  setup do
    @character_contact = character_contacts(:one)
  end

  test "visiting the index" do
    visit character_contacts_url
    assert_selector "h1", text: "Character Contacts"
  end

  test "creating a Character contact" do
    visit character_contacts_url
    click_on "New Character Contact"

    fill_in "Character", with: @character_contact.character_id
    fill_in "Contact", with: @character_contact.contact_id
    fill_in "Loyalty", with: @character_contact.loyalty
    click_on "Create Character contact"

    assert_text "Character contact was successfully created"
    click_on "Back"
  end

  test "updating a Character contact" do
    visit character_contacts_url
    click_on "Edit", match: :first

    fill_in "Character", with: @character_contact.character_id
    fill_in "Contact", with: @character_contact.contact_id
    fill_in "Loyalty", with: @character_contact.loyalty
    click_on "Update Character contact"

    assert_text "Character contact was successfully updated"
    click_on "Back"
  end

  test "destroying a Character contact" do
    visit character_contacts_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Character contact was successfully destroyed"
  end
end
