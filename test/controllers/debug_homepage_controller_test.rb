require 'test_helper'

class DebugHomepageControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get debug_homepage_index_url
    assert_response :success
  end

end
