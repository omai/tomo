require 'test_helper'

class CharacterContactsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @character_contact = character_contacts(:one)
  end

  test "should get index" do
    get character_contacts_url
    assert_response :success
  end

  test "should get new" do
    get new_character_contact_url
    assert_response :success
  end

  test "should create character_contact" do
    assert_difference('CharacterContact.count') do
      post character_contacts_url, params: { character_contact: { character_id: @character_contact.character_id, contact_id: @character_contact.contact_id, loyalty: @character_contact.loyalty } }
    end

    assert_redirected_to character_contact_url(CharacterContact.last)
  end

  test "should show character_contact" do
    get character_contact_url(@character_contact)
    assert_response :success
  end

  test "should get edit" do
    get edit_character_contact_url(@character_contact)
    assert_response :success
  end

  test "should update character_contact" do
    patch character_contact_url(@character_contact), params: { character_contact: { character_id: @character_contact.character_id, contact_id: @character_contact.contact_id, loyalty: @character_contact.loyalty } }
    assert_redirected_to character_contact_url(@character_contact)
  end

  test "should destroy character_contact" do
    assert_difference('CharacterContact.count', -1) do
      delete character_contact_url(@character_contact)
    end

    assert_redirected_to character_contacts_url
  end
end
