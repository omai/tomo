module XMLHelper
    class << self
        #
        # Grabs an integer value from a nokogiri Node
        #
        # @param [Nokogiri::XML::Node] nokogiri_node Node we are going to be grabbing data from
        # @param [String] xpath Xpath to the relevant node
        # @param [Fixnum] default_value Default value we should return if we don't find anything (Defaults to 0)
        #
        # @return [Fixnum] Found value, or default value if nonexistant
        #
        def grab_int_from_nokogiri(nokogiri_node, xpath, default_value=0)
            i=nokogiri_node.xpath(xpath).to_s.to_i
            i=default_value if i==0
            i
        end
        #
        # Grabs a string value from a nokogiri node
        #
        # @param [Nokogiri::XML::Node] nokogiri_node Node we are going to be grabbing data from
        # @param [String] xpath Xpath to relevant node
        # @param [String] default_value Default value we should return if we don't find anything (Defaults to '')
        #
        # @return [String] Found value, or default value if nonexistant
        #
        def grab_string_from_nokogiri(nokogiri_node, xpath, default_value='')
            s=nokogiri_node.xpath(xpath).to_s
            s=default_value if s.empty?
            s
        end
        #
        # Grabs a boolean value from a nokogiri Node
        #
        # @param [Nokogiri::XML::Node] nokogiri_node Node we are going to be grabbing data from
        # @param [String] xpath Xpath to relevant node
        # @param [Boolean] default_value Default value if we don't find anything (Defaults to false)
        #
        # @return [Boolean] Found value, or default value if nonexistant
        #
        def grab_bool_from_nokogiri(nokogiri_node, xpath, default_value=false)
            b=nokogiri_node.xpath(xpath).to_s.downcase
            return default_value if(b.nil?)
            b[0]=='t'
        end
        def node_exists(nokogiri_node,xpath)
        end
    end
end