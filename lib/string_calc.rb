module StringCalc
    #
    # Handles calculation of fixed values
    #
    # @param [String] formula Formula that we are trying to calculate those out of
    # @param [Fixnum] rating Rating of the gear that we are calucluating.
    #
    # @return [String] Calucluated Formula
    #
    def calculate_fixed_values(formula, rating=1)
        match=/FixedValues\(((\d+[RF]?,?)+)\)/.match(formula)
        return forumla if match.nil? # If we don't have a match, we know the formula is good to go as is.
        return match[1].split(',')[rating-1]
    end
    #
    # Calculates a Formula with Various varaibles subbed out
    #
    # @param [String] formula Formula to be evaluated
    # @param [Hash<String,Fixnum>] varaibles Varaibles to be subbed
    # @option varaibles [Fixnum] :Rating Rating of the item
    #
    # @return [String] Evaluated formula
    #
    def calculate_formula_with_variables(formula, varaibles={'Rating' => 1})
        # Replacing all of our varaibles
        varaibles.each do |var,value|
            formula.gsub!(/#{var}/,value.to_s)
        end
        # ok, bit of special code for any FixedValues Array
        # A bit unsafe, but should be ok seeing how we are getting the data from the listings from the Chummer5/Chummer5 repo
        eval formula
    end
end