#
# Bunch of functions to make handling listings a bit easier. and DRYer
#
module ListingHelper
    class << self
    
        #
        # Grabs a specified listing, and returns a processed Nokogiri with the contents of the listing
        #
        # @param [String] version_number Version number for the listing
        # @param [String] file_name Name for the File
        #
        # @return [Nokogiri::XML::Document] Nokogiri'd document.
        #
        def get_listing(version_number, file_name)
            # Ok, initial checks: Does the directory exist?
            raise ArgumentError.new("The Directory vendor/chummer5-listings/#{version_number} does not exist") unless File.directory? "vendor/chummer5-listings/#{version_number}"
            # Second check: Does the file exist?
            raise ArgumentError.new("The file #{file_name} does not exist in vendor/chummer5-listings/#{version_number}") unless File.exist? "vendor/chummer5-listings/#{version_number}/#{file_name}"
            # So everything exists? Sick! let's go ahead and create our Nokogiri document
            file=File.open("vendor/chummer5-listings/#{version_number}/#{file_name}", 'r')
            xml_string=file.read
            file.close
            Nokogiri::XML(xml_string)
        end
    end
end