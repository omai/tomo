# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_02_13_010003) do

  create_table "character_attributes", force: :cascade do |t|
    t.integer "character_id"
    t.integer "metatype_min"
    t.integer "metatype_max"
    t.integer "metatype_aug_max"
    t.integer "base"
    t.integer "karma"
    t.integer "metatype_category"
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["character_id"], name: "index_character_attributes_on_character_id"
  end

  create_table "character_contacts", force: :cascade do |t|
    t.integer "contact_id"
    t.integer "character_id"
    t.integer "loyalty"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "blackmail"
    t.boolean "family"
    t.index ["character_id"], name: "index_character_contacts_on_character_id"
    t.index ["contact_id"], name: "index_character_contacts_on_contact_id"
  end

  create_table "character_items", force: :cascade do |t|
    t.integer "gear_listing_id"
    t.integer "character_id"
    t.string "name"
    t.string "gear_name"
    t.integer "rating"
    t.integer "quantity"
    t.integer "availability"
    t.integer "cost"
    t.boolean "bonded"
    t.boolean "equipped"
    t.boolean "wireless_on"
    t.integer "matrix_condition_monitor_filled"
    t.integer "parent_item"
    t.text "notes"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "capacity"
    t.integer "armor_capacity"
    t.integer "min_rating"
    t.string "extra"
    t.text "bonus"
    t.text "wireless_bonus"
    t.text "armor_bonus"
    t.string "can_form_persona"
    t.integer "device_rating"
    t.string "forced_value"
    t.integer "matrix_condition_monitor_bonus"
    t.boolean "allow_rename"
    t.string "location"
    t.boolean "discounted_cost"
    t.string "program_limit"
    t.boolean "overclocked"
    t.string "attribute_array"
    t.string "attack"
    t.string "sleaze"
    t.string "data_processing"
    t.string "firewall"
    t.string "mod_attack"
    t.string "mod_sleaze"
    t.string "mod_data_processing"
    t.string "mod_firewall"
    t.string "mod_attribute_array"
    t.boolean "can_swap_attributes"
    t.boolean "active"
    t.boolean "home_node"
    t.integer "sort_order"
    t.index ["character_id"], name: "index_character_items_on_character_id"
    t.index ["gear_listing_id"], name: "index_character_items_on_gear_listing_id"
  end

  create_table "character_qualities", force: :cascade do |t|
    t.integer "quality_listing_id"
    t.integer "character_id"
    t.string "extra"
    t.text "notes"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["character_id"], name: "index_character_qualities_on_character_id"
    t.index ["quality_listing_id"], name: "index_character_qualities_on_quality_listing_id"
  end

  create_table "character_skills", force: :cascade do |t|
    t.integer "skill_listing_id"
    t.integer "character_id"
    t.boolean "is_knowledge"
    t.string "specialization"
    t.text "notes"
    t.integer "karma"
    t.integer "base"
    t.string "name"
    t.string "category"
    t.string "type"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["character_id"], name: "index_character_skills_on_character_id"
    t.index ["skill_listing_id"], name: "index_character_skills_on_skill_listing_id"
  end

  create_table "characters", force: :cascade do |t|
    t.string "name"
    t.text "data"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "alias"
    t.string "images"
  end

  create_table "contacts", force: :cascade do |t|
    t.string "name"
    t.string "location"
    t.string "archetype"
    t.text "data"
    t.boolean "visible"
    t.boolean "player_custom"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "metatype"
    t.string "sex"
    t.string "age"
    t.string "prefered_payment"
    t.string "hobbies_vice"
    t.string "personal_life"
    t.boolean "group"
    t.text "notes"
    t.integer "connection"
    t.string "contact_type"
  end

  create_table "gear_listings", force: :cascade do |t|
    t.string "gear_id"
    t.string "name"
    t.string "category"
    t.integer "rating"
    t.string "source"
    t.string "page"
    t.string "availibility"
    t.string "cost"
    t.integer "cost_for"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "capacity"
    t.string "armor_capacity"
    t.text "bonus"
    t.text "weapon_bonus"
    t.boolean "require_parent"
    t.integer "min_rating"
    t.string "add_on_categories"
    t.boolean "allow_rename"
    t.string "can_form_persona"
    t.string "device_rating"
    t.integer "matrix_condition_monitor_bonus"
    t.string "programs"
    t.string "attack"
    t.string "sleaze"
    t.string "data_processing"
    t.string "firewall"
    t.string "attribute_array"
    t.string "mod_attack"
    t.string "mod_sleaze"
    t.string "mod_data_processing"
    t.string "mod_firewall"
    t.string "mod_attribute_array"
    t.string "sub_gears"
    t.index ["gear_id"], name: "index_gear_listings_on_gear_id"
  end

  create_table "quality_listings", force: :cascade do |t|
    t.string "quality_id"
    t.string "name"
    t.integer "karma"
    t.string "category"
    t.integer "limit"
    t.text "bonus"
    t.text "forbidden"
    t.text "required"
    t.string "source"
    t.string "page"
    t.boolean "hidden"
    t.boolean "only_priority_given"
    t.string "name_on_page"
    t.text "cost_discount"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["quality_id"], name: "index_quality_listings_on_quality_id"
  end

  create_table "skill_listings", force: :cascade do |t|
    t.string "skill_id"
    t.string "name"
    t.string "skill_attribute"
    t.string "category"
    t.boolean "defaultable"
    t.string "skill_group"
    t.string "specializations"
    t.string "source"
    t.string "page"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["skill_id"], name: "index_skill_listings_on_skill_id"
  end

end
