class CreateCharacterSkills < ActiveRecord::Migration[5.2]
  def change
    create_table :character_skills do |t|
      t.references :skill_listing, foreign_key: true
      t.references :character, foreign_key: true
      t.boolean :is_knowledge
      t.string :specialization
      t.text :notes
      t.integer :karma
      t.integer :base
      t.string :name
      t.string :category
      t.string :type
      t.timestamps
    end
    remove_column :characters, :skills, :text
  end
end
