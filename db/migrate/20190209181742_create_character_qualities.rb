class CreateCharacterQualities < ActiveRecord::Migration[5.2]
  def change
    create_table :character_qualities do |t|
      t.references :quality_listing, foreign_key: true
      t.references :character, foreign_key: true
      t.string :extra
      t.text :notes

      t.timestamps
    end
    remove_column :characters, :qualities, :text
  end
end
