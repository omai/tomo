class ChangeCostInGearListingToString < ActiveRecord::Migration[5.2]
  def up
    change_column :gear_listings, :cost, :string
  end
  def down 
    change_column :gear_listings, :cost, :integer
  end
end
