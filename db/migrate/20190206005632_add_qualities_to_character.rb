class AddQualitiesToCharacter < ActiveRecord::Migration[5.2]
  def change
    add_column :characters, :qualities, :text
  end
end
