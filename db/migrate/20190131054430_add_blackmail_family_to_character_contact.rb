class AddBlackmailFamilyToCharacterContact < ActiveRecord::Migration[5.2]
  def change
    add_column :character_contacts, :blackmail, :boolean
    add_column :character_contacts, :family, :boolean
  end
end
