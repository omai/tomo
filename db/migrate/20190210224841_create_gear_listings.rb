class CreateGearListings < ActiveRecord::Migration[5.2]
  def change
    create_table :gear_listings do |t|
      t.string :gear_id
      t.string :name
      t.string :category
      t.integer :rating
      t.string :source
      t.string :page
      t.string :availibility
      t.integer :cost
      t.integer :cost_for

      t.timestamps
    end
    add_index :gear_listings, :gear_id
  end
end
