class AddAliasToCharacters < ActiveRecord::Migration[5.2]
  def change
    add_column :characters, :alias, :string
  end
end
