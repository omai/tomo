class AddAdditionalRowsToContacts < ActiveRecord::Migration[5.2]
  def change
    add_column :contacts, :metatype, :string
    add_column :contacts, :sex, :string
    add_column :contacts, :age, :string
    add_column :contacts, :prefered_payment, :string
    add_column :contacts, :hobbies_vice, :string
    add_column :contacts, :personal_life, :string
    add_column :contacts, :group, :boolean
    add_column :contacts, :notes, :text
  end
end
