class AddSkillsToCharacters < ActiveRecord::Migration[5.2]
  def change
    add_column :characters, :skills, :text
  end
end
