class CreateCharacterItems < ActiveRecord::Migration[5.2]
  def change
    create_table :character_items do |t|
      t.references :gear_listing, foreign_key: true
      t.references :character, foreign_key: true
      t.string :name
      t.string :gear_name
      t.integer :rating
      t.integer :quantity
      t.integer :availability
      t.integer :cost
      t.boolean :bonded
      t.boolean :equipped
      t.boolean :wireless_on
      t.integer :matrix_condition_monitor_filled
      t.integer :parent_item
      t.text :notes

      t.timestamps
    end
  end
end
