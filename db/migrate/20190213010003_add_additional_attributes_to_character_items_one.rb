class AddAdditionalAttributesToCharacterItemsOne < ActiveRecord::Migration[5.2]
  def change
    # Currently Implemented: gear_listings, character parent_item, rating, quantity
    #                        availibility, cost, bonded, equipped, wireless_on
    #                        matrix_condition_monitor_filled, notes
    # Needs to be Implemented: capacity, armor_capacity, min_rating, extra,
    #                          bonus (Should be text, but should change to bonus_id later)
    #                          wireless_bonus (same thing for bonus), can_form_persona, 
    #                          device_rating, gear_name, forced_value,
    #                          matrix_condition_monitor_bonus, allow_rename
    #                          location, discounted_cost, program_limit, overclocked
    #                          attack, sleaze, data_processing, firewall, attribute_array,
    #                          mod_attack, mod_sleaze, mod_data_processing, mod_firewall,
    #                          mod_attribute_array, can_swap_attributes, active, home_node,
    #                          sort_order
    # TODO:
    # Needs to be Implemented Later: parent_id (I think this means that it gets tied to other gear)
    add_column :character_items, :capacity, :integer
    add_column :character_items, :armor_capacity, :integer
    add_column :character_items, :min_rating, :integer
    add_column :character_items, :extra, :string
    add_column :character_items, :bonus, :text # Right ... this is going to need to be changed to an id later.
    add_column :character_items, :wireless_bonus, :text # Same as above
    add_column :character_items, :armor_bonus, :text # Same as above
    add_column :character_items, :can_form_persona, :string
    add_column :character_items, :device_rating, :integer
    add_column :character_items, :forced_value, :string
    add_column :character_items, :matrix_condition_monitor_bonus, :integer
    add_column :character_items, :allow_rename, :boolean
    add_column :character_items, :location, :string
    add_column :character_items, :discounted_cost, :boolean
    add_column :character_items, :program_limit, :string # Should find a decker on the hub to check and see what this is supposed to be 
    add_column :character_items, :overclocked, :boolean
    add_column :character_items, :attribute_array, :string
    add_column :character_items, :attack, :string
    add_column :character_items, :sleaze, :string
    add_column :character_items, :data_processing, :string
    add_column :character_items, :firewall, :string
    add_column :character_items, :mod_attack, :string
    add_column :character_items, :mod_sleaze, :string
    add_column :character_items, :mod_data_processing, :string
    add_column :character_items, :mod_firewall, :string
    add_column :character_items, :mod_attribute_array, :string # Should double check this somehow ...
    add_column :character_items, :can_swap_attributes, :boolean
    add_column :character_items, :active, :boolean
    add_column :character_items, :home_node, :boolean
    add_column :character_items, :sort_order, :integer
    # add_column :character_items, :gear_name, :string
    # And OHGODS THERE IS MOAR, this time for character_listing
    # Currently Implemented: name, category, rating, source, page, availibility, cost, cost_for
    # Needs to be Implemented: capacity, armor_capacity, bonus, require_parent, min_rating,
    #                          add_on_categories, allow_rename, bonus, can_form_persona, 
    #                          device_rating, matrix_condition_monitor_bonus, 
    #                          programs (I think this gets mapped to program limit?), attack,
    #                          sleaze, data_processing, firewall, attribute_array, 
    #                          mod_attack, mod_sleaze, mod_data_processing, mod_firewall, 
    #                          mod_attribute_array, can_swap_attributes, weapon_bonus, 
    # Needs to be Implemented, and need to write special code for: Subgear (GODS WHY?!)
    # TODO:
    # Needs to be Implemented Later: forbidden, wireless_bonus (need to figure out where that is...)
    add_column :gear_listings, :capacity, :string
    add_column :gear_listings, :armor_capacity, :string
    add_column :gear_listings, :bonus, :text # Welp we can expect a bonus listing in the near future (gods help us)
    add_column :gear_listings, :weapon_bonus, :text
    add_column :gear_listings, :require_parent, :boolean
    add_column :gear_listings, :min_rating, :integer
    add_column :gear_listings, :add_on_categories, :string # This is going to be an array later ...
    add_column :gear_listings, :allow_rename, :boolean
    add_column :gear_listings, :can_form_persona, :string
    add_column :gear_listings, :device_rating, :string
    add_column :gear_listings, :matrix_condition_monitor_bonus, :integer
    add_column :gear_listings, :programs, :string # Example for This needing to be a string: gear_id: 00ccda48-98a0-42b1-a997-1e1cf1f5269a
    add_column :gear_listings, :attack, :string
    add_column :gear_listings, :sleaze, :string
    add_column :gear_listings, :data_processing, :string
    add_column :gear_listings, :firewall, :string
    add_column :gear_listings, :attribute_array, :string
    add_column :gear_listings, :mod_attack, :string
    add_column :gear_listings, :mod_sleaze, :string
    add_column :gear_listings, :mod_data_processing, :string
    add_column :gear_listings, :mod_firewall, :string
    add_column :gear_listings, :mod_attribute_array, :string
    add_column :gear_listings, :sub_gears, :string # This is going to be horrific, but eseentially we need an array for this, an array of ids referencing gear_listings again
  end
end
