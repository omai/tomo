class AddCAttributesToCharacters < ActiveRecord::Migration[5.2]
  def change
    add_column :characters, :c_attributes, :string
  end
end
