class CreateSkillListings < ActiveRecord::Migration[5.2]
  def change
    create_table :skill_listings do |t|
      t.string :skill_id
      t.string :name
      t.string :skill_attribute
      t.string :category
      t.boolean :defaultable
      t.string :skill_group
      t.string :specializations
      t.string :source
      t.string :page

      t.timestamps
    end
    add_index :skill_listings, :skill_id
  end
end
