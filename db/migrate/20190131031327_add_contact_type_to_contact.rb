class AddContactTypeToContact < ActiveRecord::Migration[5.2]
  def change
    add_column :contacts, :contact_type, :string
  end
end
