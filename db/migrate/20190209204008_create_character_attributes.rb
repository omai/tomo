class CreateCharacterAttributes < ActiveRecord::Migration[5.2]
  def change
    create_table :character_attributes do |t|
      t.references :character, foreign_key: true
      t.integer :metatype_min
      t.integer :metatype_max
      t.integer :metatype_aug_max
      t.integer :base
      t.integer :karma
      t.integer :metatype_category
      t.string :name

      t.timestamps
      remove_column :characters, :c_attributes, :string
    end
  end
end
