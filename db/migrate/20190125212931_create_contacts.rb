class CreateContacts < ActiveRecord::Migration[5.2]
  def change
    create_table :contacts do |t|
      t.string :name
      t.string :location
      t.string :archetype
      t.text :data
      t.boolean :visible
      t.boolean :player_custom

      t.timestamps
    end
  end
end
