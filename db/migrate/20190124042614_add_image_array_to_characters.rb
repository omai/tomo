class AddImageArrayToCharacters < ActiveRecord::Migration[5.2]
  def change
    add_column :characters, :images, :string
  end
end
