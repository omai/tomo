class AddConnectionToContacts < ActiveRecord::Migration[5.2]
  def change
    add_column :contacts, :connection, :integer
  end
end
