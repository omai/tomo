class CreateCharacterContacts < ActiveRecord::Migration[5.2]
  def change
    create_table :character_contacts do |t|
      t.references :contact, foreign_key: true
      t.references :character, foreign_key: true
      t.integer :loyalty

      t.timestamps
    end
  end
end
