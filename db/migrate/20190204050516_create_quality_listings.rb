class CreateQualityListings < ActiveRecord::Migration[5.2]
  def change
    create_table :quality_listings do |t|
      t.string :quality_id
      t.string :name
      t.integer :karma
      t.string :category
      t.integer :limit
      t.text :bonus
      t.text :forbidden
      t.text :required
      t.string :source
      t.string :page
      t.boolean :hidden
      t.boolean :only_priority_given
      t.string :name_on_page
      t.text :cost_discount

      t.timestamps
    end
    add_index :quality_listings, :quality_id
  end
end
