# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

# Ok first of all we need to go ahead and grab the latest version that in vendor/chummer5-listings
match=/\/([^\/]+$)/.match(Dir['vendor/chummer5-listings/*'][-1])
unless(match.nil?)
    # Ok Cool, there is a version of this that exists, neat.
    # Let's go ahead and load the listings then.
    current_version=match[1]
    GearListing.load_listings(current_version)
    QualityListing.load_listings(current_version)
    SkillListing.load_listings(current_version)
end