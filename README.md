[![Made with Ruby 2.5.4](https://img.shields.io/badge/made%20with-Ruby%202.5.4-CC342D.svg)](https://www.ruby-lang.org/en/) 
[![Made with Rails 5.2.2](https://img.shields.io/badge/made%20with-Rails%205.2.2-cc0000.svg)](https://rubyonrails.org/)
[![Made with Node.js 11.6.0](https://img.shields.io/badge/made%20with-Node.js%2011.6.0-026e00.svg)](https://nodejs.org/)

# Config/Install

## Linux/macOS

1. Install [RVM](https://rvm.io/rvm/install)
2. Install ruby version 2.5.4 by running the command `rvm install 2.5.4`
3. Install [Node.js](https://nodejs.org/en/download/) (or if you wanna do it through a [Package manager](https://nodejs.org/en/download/package-manager/))
4. Install [Yarn](https://yarnpkg.com/lang/en/docs/install/#debian-stable)
5. Install packages by running `bundle install`
6. Set up database by running `rails db:setup`
7. Have a drink you are done with setup!

## Windows

1. Install [Rails via the Rails installer](http://railsinstaller.org/en)
2. Install [Node.js](https://nodejs.org/en/download/) (or if you wanna do it through a [Package manager](https://nodejs.org/en/download/package-manager/))
3. Install [Yarn](https://yarnpkg.com/lang/en/docs/install/#windows-stable)
5. Install packages by running `bundle install`
6. Set up database by running `rails db:setup`
7. Have a drink you are done with setup!

# Running the Development enviroment

1. Run `./bin/webpack-dev-server --host 127.0.0.1`
2. In a seperate Terminal run `rails s`
3. Connect to `localhost:3000` to get into your local instance