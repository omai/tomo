Rails.application.routes.draw do
  get 'available_skills_debug/index'
  resources :character_contacts
  resources :contacts
  get 'debug_homepage/index'
  resources :characters
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  get "/chummer_download", to:'chummer_download#index'
  post "/chummer_download/download", to: 'chummer_download#download'
  resources :characters
  get "/character/upload", to:"characters#upload_user_side"
  post "/character/upload", to:"characters#upload"
  get '/listings/skills', to:'listing_debug#skills'
  get '/listings/qualities', to:'listing_debug#qualities'
  get '/listings/items', to:'listing_debug#items'
  root to: 'debug_homepage#index'
end
